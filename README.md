# ParsePDM
在 Mac OS 上查看 PDM 文件

## 使用方式
1. 把项目clone到本地
2. 打开 dist --> ParsePDM.jar

## 原项目地址https://github.com/smshen/ParsePDM.git

## 改进部分
1. 按照英语表名进行排序加载
2. 将原来是否为空的解析a:Mandatory修改为a:Column.Mandatory
3. 选择表或者行时会把表名或列名复制到剪切板中
4. 文件打开时会将上次打开的路径保存到全局变量中，只需要第一次指定目录，你也可以自己指定一个默认目录
